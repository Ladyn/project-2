let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");
//retrieve the userId stored in our local storage
let userId = localStorage.getItem("id");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`http://localhost:3000/api/courses/${courseId}`)
.then((res) => res.json())
.then((data) => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
})
	
